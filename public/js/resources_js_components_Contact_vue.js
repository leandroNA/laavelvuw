"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_components_Contact_vue"],{

/***/ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Contact.vue?vue&type=template&id=4c2584f6&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Contact.vue?vue&type=template&id=4c2584f6& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* binding */ render),
/* harmony export */   "staticRenderFns": () => (/* binding */ staticRenderFns)
/* harmony export */ });
var render = function render() {
  var _vm = this,
    _c = _vm._self._c;
  return _c("div", {
    staticClass: "container"
  }, [_c("br"), _vm._v(" "), _c("div", {
    staticClass: "contact__wrapper shadow-lg mt-n9"
  }, [_c("div", {
    staticClass: "row no-gutters"
  }, [_c("div", {
    staticClass: "col-lg-5 contact-info__wrapper gradient-brand-color p-5 order-lg-2"
  }, [_c("h3", {
    staticClass: "color--white mb-5"
  }, [_vm._v(" ")]), _vm._v(" "), _vm._m(0), _vm._v(" "), _c("figure", {
    staticClass: "figure position-absolute m-0 opacity-06 z-index-100",
    staticStyle: {
      bottom: "0",
      right: "10px"
    }
  }, [_c("svg", {
    attrs: {
      xmlns: "http://www.w3.org/2000/svg",
      "xmlns:xlink": "http://www.w3.org/1999/xlink",
      width: "444px",
      height: "626px"
    }
  }, [_c("defs", [_c("linearGradient", {
    attrs: {
      id: "PSgrad_1",
      x1: "0%",
      x2: "81.915%",
      y1: "57.358%",
      y2: "0%"
    }
  }, [_c("stop", {
    attrs: {
      offset: "0%",
      "stop-color": "rgb(255,255,255)",
      "stop-opacity": "1"
    }
  }), _vm._v(" "), _c("stop", {
    attrs: {
      offset: "100%",
      "stop-color": "rgb(0,54,207)",
      "stop-opacity": "0"
    }
  })], 1)], 1)])])]), _vm._v(" "), _vm._m(1)])])]);
};
var staticRenderFns = [function () {
  var _vm = this,
    _c = _vm._self._c;
  return _c("ul", {
    staticClass: "contact-info__list list-style--none position-relative z-index-101"
  }, [_c("li", {
    staticClass: "mb-4 pl-4"
  }, [_c("span", {
    staticClass: "position-absolute"
  }, [_c("i", {
    staticClass: "fas fa-envelope"
  }), _vm._v(" support@example.com")])]), _vm._v(" "), _c("li", {
    staticClass: "mb-4 pl-4"
  }, [_c("span", {
    staticClass: "position-absolute"
  }, [_c("i", {
    staticClass: "fas fa-phone"
  }), _vm._v(" 300 258 4545")])]), _vm._v(" "), _c("li", {
    staticClass: "mb-4 pl-4"
  }, [_c("span", {
    staticClass: "position-absolute"
  }, [_c("i", {
    staticClass: "fas fa-map-marker-alt"
  }), _vm._v("\n                            Bogota D.C \n                        ")])])]);
}, function () {
  var _vm = this,
    _c = _vm._self._c;
  return _c("div", {
    staticClass: "col-lg-7 contact-form__wrapper p-5 order-lg-1"
  }, [_c("form", {
    staticClass: "contact-form form-validate",
    attrs: {
      action: "#"
    }
  }, [_c("div", {
    staticClass: "row"
  }, [_c("div", {
    staticClass: "col-sm-6 mb-3"
  }, [_c("div", {
    staticClass: "form-group"
  }, [_c("label", {
    staticClass: "required-field",
    attrs: {
      "for": "firstName"
    }
  }, [_vm._v("Nombre")]), _vm._v(" "), _c("input", {
    staticClass: "form-control",
    attrs: {
      type: "text",
      id: "firstName",
      name: "firstName",
      placeholder: "Nombre"
    }
  })])]), _vm._v(" "), _c("div", {
    staticClass: "col-sm-6 mb-3"
  }, [_c("div", {
    staticClass: "form-group"
  }, [_c("label", {
    attrs: {
      "for": "lastName"
    }
  }, [_vm._v("Apellido")]), _vm._v(" "), _c("input", {
    staticClass: "form-control",
    attrs: {
      type: "text",
      id: "lastName",
      name: "lastName",
      placeholder: "Apellido"
    }
  })])]), _vm._v(" "), _c("div", {
    staticClass: "col-sm-6 mb-3"
  }, [_c("div", {
    staticClass: "form-group"
  }, [_c("label", {
    staticClass: "required-field",
    attrs: {
      "for": "email"
    }
  }, [_vm._v("Email")]), _vm._v(" "), _c("input", {
    staticClass: "form-control",
    attrs: {
      type: "text",
      id: "email",
      name: "email",
      placeholder: "correo@example.com"
    }
  })])]), _vm._v(" "), _c("div", {
    staticClass: "col-sm-6 mb-3"
  }, [_c("div", {
    staticClass: "form-group"
  }, [_c("label", {
    attrs: {
      "for": "phone"
    }
  }, [_vm._v("Numero de Celular")]), _vm._v(" "), _c("input", {
    staticClass: "form-control",
    attrs: {
      type: "tel",
      id: "phone",
      name: "phone",
      placeholder: "300 222 2222"
    }
  })])]), _vm._v(" "), _c("div", {
    staticClass: "col-sm-12 mb-3"
  }, [_c("div", {
    staticClass: "form-group"
  }, [_c("label", {
    staticClass: "required-field",
    attrs: {
      "for": "message"
    }
  }, [_vm._v("Como te podemos ayudar?")]), _c("textarea", {
    staticClass: "form-control",
    attrs: {
      id: "message",
      name: "message",
      rows: "4",
      placeholder: "Explicanos tu situacion"
    }
  })])]), _vm._v(" "), _c("div", {
    staticClass: "col-sm-12 mb-3"
  }, [_c("button", {
    staticClass: "btn btn-primary",
    attrs: {
      type: "submit",
      name: "submit"
    }
  }, [_vm._v("\n                                Enviar\n                            ")])])])])]);
}];
render._withStripped = true;


/***/ }),

/***/ "./resources/js/components/Contact.vue":
/*!*********************************************!*\
  !*** ./resources/js/components/Contact.vue ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Contact_vue_vue_type_template_id_4c2584f6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Contact.vue?vue&type=template&id=4c2584f6& */ "./resources/js/components/Contact.vue?vue&type=template&id=4c2584f6&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! !../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");

var script = {}


/* normalize component */
;
var component = (0,_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  script,
  _Contact_vue_vue_type_template_id_4c2584f6___WEBPACK_IMPORTED_MODULE_0__.render,
  _Contact_vue_vue_type_template_id_4c2584f6___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns,
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/Contact.vue"
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (component.exports);

/***/ }),

/***/ "./resources/js/components/Contact.vue?vue&type=template&id=4c2584f6&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/Contact.vue?vue&type=template&id=4c2584f6& ***!
  \****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "render": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Contact_vue_vue_type_template_id_4c2584f6___WEBPACK_IMPORTED_MODULE_0__.render),
/* harmony export */   "staticRenderFns": () => (/* reexport safe */ _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Contact_vue_vue_type_template_id_4c2584f6___WEBPACK_IMPORTED_MODULE_0__.staticRenderFns)
/* harmony export */ });
/* harmony import */ var _node_modules_babel_loader_lib_index_js_clonedRuleSet_5_use_0_node_modules_vue_loader_lib_loaders_templateLoader_js_ruleSet_1_rules_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Contact_vue_vue_type_template_id_4c2584f6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!../../../node_modules/vue-loader/lib/index.js??vue-loader-options!./Contact.vue?vue&type=template&id=4c2584f6& */ "./node_modules/babel-loader/lib/index.js??clonedRuleSet-5.use[0]!./node_modules/vue-loader/lib/loaders/templateLoader.js??ruleSet[1].rules[2]!./node_modules/vue-loader/lib/index.js??vue-loader-options!./resources/js/components/Contact.vue?vue&type=template&id=4c2584f6&");


/***/ })

}]);