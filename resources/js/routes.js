const Home = () => import('./components/Home.vue')
const Contact = () => import('./components/Contact.vue')



const Show = () => import('./components/post/Show.vue')
const Edit = () => import('./components/post/Edit.vue')
const Create = () => import('./components/post/Create.vue')


export const routes = [
    {
        name: 'home',
        path: '/',
        component:Home
    },
    {
        name: 'contact',
        path: '/contacto',
        component:Contact
    },
    {
        name: 'show',
        path: '/mostrar',
        component:Show
    },
    {
        name: 'edit',
        path: '/editar/:id',
        component:Edit
    },
    {
        name: 'create',
        path: '/crear',
        component:Create
    }

]
