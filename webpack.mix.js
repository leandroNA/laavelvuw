const mix = require('laravel-mix');
const nodeExternals = require('webpack-node-externals');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js').vue()
    .postCss('resources/css/app.css', 'public/css', [
        //
        
    ]);

mix.disableSuccessNotifications();

  mix
  .webpackConfig({ resolve: {fallback: { "path": false,
                                        "stream": false,
                                        "http": false,
                                        "crypto": false,
                                        "zlib": false,
                                        "zlib": false,
                                        "async_hooks": false,
                                        "fs": false,
                                        "net": false, },
                                         
                                        // externals: {
                                    // express: require'express',
                                    // },
                                    },
                                    target: 'node',
                                    externals: {
                                        express: 'express',
                                      },
                                    
    })